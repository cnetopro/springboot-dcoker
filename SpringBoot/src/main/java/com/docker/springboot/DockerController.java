package com.docker.springboot;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
@ResponseBody
public class DockerController {
    @RequestMapping("/")
    public String hello(){
        return "Hello SpringBoot Docker";
    }
}
